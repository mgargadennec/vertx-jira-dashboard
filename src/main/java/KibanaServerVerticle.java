import org.vertx.java.core.Handler;
import org.vertx.java.core.file.impl.PathAdjuster;
import org.vertx.java.core.http.HttpServer;
import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.platform.Verticle;

public class KibanaServerVerticle extends Verticle {

  public void start() {
      HttpServer server = vertx.createHttpServer();

      server.requestHandler(new Handler<HttpServerRequest>() {
          public void handle(HttpServerRequest req) {
              String file = "";
              if (req.path().equals("/")) {
                  file = "index.html";
              } else if (!req.path().contains("..")) {
                  file = req.path().substring(1,req.path().length());
              }
              container.logger().info("Sending file " + file);
              req.response().sendFile(file);
          }
      }).listen(1234, "localhost");

      container.logger().info("KibanaServerVerticle has Started");
  }
}
