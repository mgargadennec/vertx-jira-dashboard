import org.vertx.java.core.Handler;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.http.HttpClient;
import org.vertx.java.core.http.HttpClientRequest;
import org.vertx.java.core.http.HttpClientResponse;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Verticle;

import java.util.Iterator;

public class JiraFinderVerticle extends Verticle {
    private HttpClient client;

    public void start() {

    client = vertx.createHttpClient()
            .setSSL(container.config().getBoolean("ssl", false))
            .setTrustAll(true)
            .setPort(container.config().getNumber("port").intValue())
            .setHost(container.config().getString("host"));

     vertx.eventBus().registerHandler("pull-all-issues", new Handler<Message>() {
         @Override
         public void handle(Message event) {
             container.logger().info("Pulling all jira issues.");

             HttpClientRequest request = client.get(container.config().getString("uri_search")+"?fields=key", new Handler<HttpClientResponse>() {
                 public void handle(HttpClientResponse resp) {
                     container.logger().info("Got a response: " + resp.statusCode());

                     resp.bodyHandler(new Handler<Buffer>() {
                         @Override
                         public void handle(Buffer event) {
                             String body = event.toString();
                             int total = new JsonObject(body).getInteger("total");

                             int maxResults = 1000;
                             int i = 0;
                             while(i<total){

                                vertx.eventBus().send("pull-issues-from-to",new JsonObject()
                                        .putNumber("startAt", i)
                                        .putNumber("maxResults", maxResults));
                                 i+=maxResults;
                             }
                         }
                     });
                 }
             });
             request.end();
         }
     });


    vertx.eventBus().registerHandler("pull-issues-from-to", new Handler<Message<JsonObject>>() {
        @Override
        public void handle(Message<JsonObject> event) {
            int startAt = event.body().getNumber("startAt").intValue() ;
            int maxResults =event.body().getNumber("maxResults").intValue() ;
            container.logger().info("Looking for jira issues from index "+startAt+" and with maxResults "+maxResults);

            HttpClientRequest request = client.get(container.config().getString("uri_search")+"?fields=key&maxResults="+maxResults+"&startAt="+startAt, new Handler<HttpClientResponse>() {
                public void handle(HttpClientResponse resp) {
                    resp.bodyHandler(new Handler<Buffer>() {
                        @Override
                        public void handle(Buffer event) {
                            String body = event.toString();
                            JsonArray issues = new JsonObject(body).getArray("issues");
                            Iterator it = issues.iterator();
                            while(it.hasNext()){
                                String key = ((JsonObject)it.next()).getString("key");
                                vertx.eventBus().send("pull-distinct-issue",key);
                            }
                        }
                    });
                }
            });
            request.end();
        }
    });


    container.logger().info("JiraFinderVerticle has Started");
  }


}
