import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.node.Node;
import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Verticle;
import static org.elasticsearch.node.NodeBuilder.*;


/**
 * Created by mael on 16/02/14.
 */
public class ElasticsearchVerticle extends Verticle{
    private Node node;
    private Client client;

    @Override
    public void start() {
        node = nodeBuilder().node();
        client = node.client();

        vertx.eventBus().registerHandler("index-issue",new Handler<Message<JsonObject>>() {
            @Override
            public void handle(Message<JsonObject> event) {
                IndexRequestBuilder rq = client.prepareIndex("jira","issue");
                rq.setSource(event.body().toString());
                rq.get();
            }
        });
        container.logger().info("Elasticsearch Verticle has started");
    }
}
