import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.vertx.java.core.Handler;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.http.*;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Verticle;

import java.util.Iterator;

public class JiraIndexerVerticle extends Verticle {
    private HttpClient client;

    public void start() {

     client = vertx.createHttpClient()
             .setSSL(container.config().getBoolean("ssl", false))
             .setTrustAll(true)
             .setPort(container.config().getNumber("port").intValue())
             .setHost(container.config().getString("host"));


     vertx.eventBus().registerHandler("pull-distinct-issue", new Handler<Message<String>>() {
         @Override
         public void handle(final Message<String> event) {
             container.logger().info("Pulling issue with key "+event.body());
             HttpClientRequest request = client.get(container.config().getString("uri_issue") +'/'+ event.body() + "/worklog", new Handler<HttpClientResponse>() {
                 public void handle(HttpClientResponse resp) {
                     resp.bodyHandler(new Handler<Buffer>() {
                         @Override
                         public void handle(Buffer body) {
                             JsonObject issue = new JsonObject(body.toString());
                             JsonArray worklogs = issue.getArray("worklogs");
                             Iterator it = worklogs.iterator();
                             while(it.hasNext()){
                                 Object worklog = it.next();

                                 if(worklog instanceof JsonObject){
                                     JsonObject copy = ((JsonObject)worklog).copy();
                                     copy.putString("issueId", event.body());
                                     copy.putNumber("timeSpentHours", copy.getNumber("timeSpentSeconds").doubleValue() / 3600);
                                     vertx.eventBus().send("index-issue",copy);
                                 }
                             }

                         }
                     });
                 }
             });
             request.end();
         }
     });


     container.logger().info("JiraIndexerVerticle has Started");
  }


}
